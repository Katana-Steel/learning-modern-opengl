#pragma once
#ifndef MODERN_OGL_L4_SCREEN
#define MODERN_OGL_L4_SCREEN

#include <iostream>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

class Camera;

class GLFW
{
    GLFWwindow* window;
    glm::vec2 last_mouse = {0.0f,0.0f};
    int screenWidth;
    int screenHeight;
    bool keys[1024] = {false};

public:
    GLFW();
    GLFW(int width, int height);
    ~GLFW();

    GLFWwindow* getWindow() const {return window;};
    int getScreenWidth() const {return screenWidth;};
    int getScreenHeight() const {return screenHeight;};
    GLfloat getAspect() const;
    void swapBuffer() const {glfwSwapBuffers(window);};
    bool stillActive() const {return (0 == glfwWindowShouldClose(window));};
    void setCallbacks(Camera *cam);
    const bool* getState() const {return keys;};
    void updateLast(glm::vec2 mouse_pos) {last_mouse = mouse_pos;};
    glm::vec2 getLast() const {return last_mouse;};
};
#endif
