#pragma once
#ifndef MODERN_OGL_L2_SHADER
#define MODERN_OGL_L2_SHADER

#include <GL/glew.h>
#include <string>
#include <glm/glm.hpp>

class Shader
{
    GLuint program;
    GLuint compileShaderSource(const std::string&, GLenum) const;
public:
    Shader() : program(0) {};
    Shader(const char* vertPath, const char* fragPath);
    void use() const { glUseProgram(program);};
    ~Shader() {glDeleteProgram(program);};
    GLint lookupUniform(const char *name) const;
    GLint lookupAttrib(const char *name) const;
    void setMatrix(const char *name, const glm::mat4 &m) const;
    void setVec3(const char *name, const glm::vec3 &v) const;
    void setFloat(const char *name, float f) const;
    void setTextureID(const char *name, GLuint id) const;
};

#endif
