#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Screen.hpp"      // Defines GLFW class for rendering RAII
#include "Shader.hpp"      // shader class that combines Vertex and fragment shaders
#include "VertexData.hpp"  // holds references and copies data to opengl buffer
#include "ImageTex.hpp"    // loads texture data and holds opengl references
#include "Model.hpp"
#include "Camera.hpp"

int main(int p, const char **src)
{
    GLFW gl{1280,720};
    glfwMakeContextCurrent(gl.getWindow());
    glewExperimental = GL_TRUE;
    if(GLEW_OK != glewInit())
    {
        std::cout << "Failed to initialize GLEW\n";
        return 1;
    }
    Camera cam{{0.0f,0.0f,6.0f}};
    glm::vec3 lightPos{1.2f, 1.0f, 2.0f};
    gl.setCallbacks(&cam);
    glViewport(0, 0, gl.getScreenWidth(), gl.getScreenHeight());

    glEnable(GL_DEPTH_TEST);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // openGL setup
    Shader lightShader{"shaders/core.vs.glsl", "shaders/light.fs.glsl"};
    Shader colorShader{"shaders/core.vs.glsl", "shaders/color.fs.glsl"};
    glm::mat4 project = glm::perspective(cam.getZoomFactor(), gl.getAspect(), 0.1f, 1000.0f);
    lightShader.setMatrix("projection", project);
    colorShader.setMatrix("projection", project);
    colorShader.setMatrix("model", glm::translate(glm::mat4{1.0f}, glm::vec3{0.0f}));
    colorShader.setVec3("light.ambient", glm::vec3{0.2f});
    colorShader.setVec3("light.diffuse", glm::vec3{0.5f});
    colorShader.setVec3("light.specular", glm::vec3{1.0f});
    colorShader.setFloat("mat.shine", 32.0f);
    colorShader.setTextureID("mat.diffuse", 0);
    colorShader.setTextureID("mat.specular", 1);

    Model m{};
    m.load("models/normal_tex_cube.mdl");

    VertexData box{m.count(), m.stride(), m.vertices.data()};
    size_t off{m.v};
    box.setShaderData({1, m.c, off});
    off += m.c;
    box.setShaderData({2, m.t, off});

    Texture diffImg{};
    diffImg.loadImageData("images/metalbox.png");
    Texture specImg{};
    specImg.loadImageData("images/box_specular.png");

    double deltaTime=0.0, lfs = 0.0;
    while(gl.stillActive())
    {
        auto fs = glfwGetTime();
        deltaTime = fs - lfs;
        lfs = fs;
        glfwPollEvents();
        glm::mat4 light_rot = glm::rotate(glm::mat4{1.0f}, (float)(0.5*deltaTime), glm::vec3{1.0f, 0.0f, 0.0f});
        lightPos = light_rot * glm::vec4{lightPos, 1.0f};
        cam.doMovement(gl.getState(), deltaTime);
        glm::mat4 view = cam.getViewMatrix();

        glClearColor(0.1f,0.1f,0.1f,1.0f);
        glClear(GL_DEPTH_BUFFER_BIT|GL_COLOR_BUFFER_BIT);
        // drawing

        colorShader.use();
        colorShader.setMatrix("view", view);
        colorShader.setVec3("light.pos", lightPos);
        colorShader.setVec3("viewPos", cam.getPosition());
        diffImg.activate(GL_TEXTURE0);
        specImg.activate(GL_TEXTURE1);

        box.bindData();
        glDrawArrays(GL_TRIANGLES, 0, m.count());
        box.unbindData();

        glBindTexture(GL_TEXTURE_2D, 0);

        lightShader.use();
        lightShader.setMatrix("view", view);
        lightShader.setMatrix(
            "model",
            glm::scale(glm::translate(glm::mat4{1.0f}, lightPos), glm::vec3{0.2f})
        );

        box.bindData();
        glDrawArrays(GL_TRIANGLES, 0, m.count());
        box.unbindData();

        gl.swapBuffer();
    }

    return 0;
}
