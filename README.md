My foolish attempt at getting better at opengl again by
following along the video series published by humble bundle called:

Modern OpenGL C++ 3D Game Tutorial Series & 3D Rendering
-------

Which is using:
* opengl 3.3 core
* glfw3
* glm
* ~~SOIL2~~ ImLib2

Simple Build steps
------
```bash
cmake -S . -B build -G Ninja
cmake --build build
build/m_o_gl
```

[Demo](https://youtu.be/hLnpBPJXVzQ)
