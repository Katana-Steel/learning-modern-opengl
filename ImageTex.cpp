#include "ImageTex.hpp"
#include <Imlib2.h>

Texture::~Texture()
{
    glDeleteTextures(1, &texture);
}

Texture::Texture()
    : width(0), height(0)
{
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    // tile repeat
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // set linear filtering
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_NEAREST);
}

void
Texture::loadImageData(const char *fname)
{
    auto image = imlib_load_image(fname);
    imlib_context_set_image(image);
    width = imlib_image_get_width();
    height = imlib_image_get_height();
    uint32_t *data = imlib_image_get_data_for_reading_only();
    glBindTexture(GL_TEXTURE_2D, texture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_BGRA, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);
    imlib_context_set_image(image);
    imlib_free_image();
}

void
Texture::activate(GLenum tex) const
{
    glActiveTexture(tex);
    glBindTexture(GL_TEXTURE_2D, texture);
}
