#include "VertexData.hpp"

VertexData::~VertexData() 
{
    if(VAO)
        glDeleteVertexArrays(1, &VAO);
    if(VBO)
        glDeleteBuffers(1, &VBO);
    if(EBO)
        glDeleteBuffers(1, &EBO);
}

void
VertexData::addIndexArray(size_t count, const GLuint *indices)
{
    glGenBuffers(1, &EBO);

    bindData();

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, count*sizeof(indices[0]), indices, GL_STATIC_DRAW);

    unbindData();
}

VertexData::VertexData(size_t count, size_t vertSize, const GLfloat *verts) :
    counts(count), stride(vertSize), EBO(0)
{
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    bindData();

    glBufferData(GL_ARRAY_BUFFER, counts*stride*sizeof(GLfloat), verts, GL_STATIC_DRAW);
    setShaderData({0,3,0});

}

void VertexData::setShaderData(const shaderLayout &layout) const
{
    bindData();
    glVertexAttribPointer(layout.loc, layout.dataSize, GL_FLOAT,GL_FALSE, stride*sizeof(GLfloat), (GLvoid*)(layout.offset*sizeof(GLfloat)));
    glEnableVertexAttribArray( layout.loc );
    unbindData();
}

void
VertexData::bindData() const
{
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
}

void
VertexData::unbindData() const
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}
