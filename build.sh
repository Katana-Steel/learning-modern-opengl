#!/bin/bash

for src in *.cpp; do
  g++ -Wall --std=c++2a -O3 -c $src &
done
wait
g++ --std=c++2a -O3 *.o -lGL -lGLEW -lglfw -lImlib2
