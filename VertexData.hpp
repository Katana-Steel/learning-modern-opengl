#pragma once
#ifndef MODERN_OGL_L2_VERTEXDATA
#define MODERN_OGL_L2_VERTEXDATA
#include <GL/glew.h>
#include <vector>

struct shaderLayout {
    GLuint loc;
    size_t dataSize; // 1, 2, 3 or 4
    size_t offset;
};

class VertexData
{
    size_t counts;
    size_t stride;
    GLuint VAO;
    GLuint VBO;
    GLuint EBO;
public:
    VertexData(size_t count, size_t vertSize, const GLfloat *verts);
    ~VertexData();
    void bindData() const;
    void unbindData() const;

    void setShaderData(const shaderLayout &layout) const;
    void addIndexArray(size_t count, const GLuint *indices);
};

#endif
