#include "Camera.hpp"
#include <GLFW/glfw3.h>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

Camera::Camera(glm::vec3 position, glm::vec3 up, GLfloat yaw, GLfloat pitch)
    : Camera()
{
    this->pos = position;
    this->up = up;
    this->yaw = yaw;
    this->pitch = pitch;
    updateCameraVectors();
}

Camera::Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw, GLfloat pitch)
    : Camera({posX,posY,posZ},{upX,upY,upZ}, yaw, pitch)
{}

glm::mat4
Camera::getViewMatrix() const
{
    return glm::lookAt(pos, pos+front, view_up);
}

void
Camera::updatePosition(C_Move direction, GLfloat deltaTime) 
{
    GLfloat velocity = this->speed * deltaTime;
    switch(direction) {
        case FORWARD:
            pos += front * velocity;
            break;
        case BACKWARD:
            pos -= front * velocity;
            break;
        case LEFT:
            pos -= right * velocity;
            break;
        case RIGHT:
            pos += right * velocity;
            break;
        case UP:
            pos -= view_up * velocity;
            break;
        case DOWN:
            pos += view_up * velocity;
            break;
    }
}

void
Camera::updateLookAt(GLfloat offset_x, GLfloat offset_y, bool pitch_lock)
{
    glm::vec2 yp{yaw,pitch};
    glm::vec2 sens{offset_x, offset_y};
    glm::vec2 min{-360.0f, -89.0f};
    glm::vec2 max{360.0f, 89.0f};

    sens *= mouse;
    yp.x += sens.x;
    yp.y += sens.y;
    float of = glm::abs(yp.x) - 360.0f;
    if (yp.x > 360.0f)
        yp.x = -360.0f + of;
    if (yp.x < -360.0f)
        yp.x = 360.0f - of;
    if(pitch_lock)
    {
        yp = glm::clamp(yp, min, max);
    }

    yaw = yp.x;
    pitch = yp.y;
    updateCameraVectors();
}

void
Camera::updateZoom(GLfloat scroll_offset)
{
    if(zoom >= 1.0f && zoom <= 45.0f)
        zoom -= scroll_offset;
    if(zoom < 1.0f)
        zoom = 1.0f;
    if(zoom > 45.0f)
        zoom = 45.0f;
    updateCameraVectors();
}

void
Camera::updateCameraVectors()
{
    glm::vec3 tempFront{};
    tempFront.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    tempFront.y = sin(glm::radians(pitch));
    tempFront.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    front = glm::normalize(tempFront);
    right = glm::normalize(glm::cross(front, up));
    view_up = glm::normalize(glm::cross(-right, front));
}

void
Camera::doMovement(const bool *keys, GLfloat deltaTime)
{
    if(keys[GLFW_KEY_W] || keys[GLFW_KEY_UP])
        updatePosition(FORWARD, deltaTime);
    if(keys[GLFW_KEY_S] || keys[GLFW_KEY_DOWN])
        updatePosition(BACKWARD, deltaTime);
    if(keys[GLFW_KEY_A] || keys[GLFW_KEY_LEFT])
        updatePosition(LEFT, deltaTime);
    if(keys[GLFW_KEY_D] || keys[GLFW_KEY_RIGHT])
        updatePosition(RIGHT, deltaTime);
    if(keys[GLFW_KEY_R] || keys[GLFW_KEY_PAGE_UP])
        updatePosition(UP, deltaTime);
    if(keys[GLFW_KEY_F] || keys[GLFW_KEY_PAGE_DOWN])
        updatePosition(DOWN, deltaTime);
}
