#pragma once
#ifndef MODERN_OGL_L5_CAMERA
#define MODERN_OGL_L5_CAMERA

#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>

enum C_Move
{
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT,
    UP,
    DOWN
};

const GLfloat cam_yaw = -90.0f;
const GLfloat cam_pitch = 0.0f;
const GLfloat cam_speed = 6.0f;
const GLfloat cam_mouse = 0.15f;
const GLfloat cam_zoom = 45.0f;

class Camera
{
    glm::vec3 front = {0.0f,0.0f,1.0f};
    glm::vec3 right = {-1.0f,0.0f,0.0f};
    glm::vec3 pos = {0.0f,0.0f,0.0f};
    glm::vec3 up = {0.0f,1.0f,0.0f};
    glm::vec3 view_up = {0.0f,1.0f,0.0f};
    GLfloat speed = cam_speed;
    GLfloat mouse = cam_mouse;
    GLfloat zoom  = cam_speed;
    void updateCameraVectors();
public:
    GLfloat yaw = cam_yaw;
    GLfloat pitch = cam_pitch;
    Camera() = default;
    Camera(glm::vec3 position, glm::vec3 up={0.0f,1.0f,0.0f}, GLfloat yaw=cam_yaw, GLfloat pitch=cam_pitch);
    Camera(GLfloat posX, GLfloat posY, GLfloat posZ, GLfloat upX, GLfloat upY, GLfloat upZ, GLfloat yaw=cam_yaw, GLfloat pitch=cam_pitch);

    glm::mat4 getViewMatrix() const;
    glm::vec3 getPosition() const {return pos;};
    void updatePosition(C_Move direction, GLfloat deltaTime);
    void updateLookAt(GLfloat offset_x, GLfloat offset_y, bool pitch_lock=true);
    void updateZoom(GLfloat scroll_offset);
    GLfloat getZoomFactor(){return zoom;};
    void doMovement(const bool *keys, GLfloat deltaTime);

};

#endif