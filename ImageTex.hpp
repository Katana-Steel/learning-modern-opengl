#pragma once
#ifndef MODERN_OGL_L3_IMAGETEX
#define MODERN_OGL_L3_IMAGETEX

#include <GL/glew.h>

class Texture
{
    GLuint texture;
    int width;
    int height;
public:
    Texture();
    ~Texture();
    void loadImageData(const char *fname);
    void activate(GLenum tex) const;
};

#endif
