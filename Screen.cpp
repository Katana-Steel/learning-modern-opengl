#include "Camera.hpp"
#include "Screen.hpp"

GLFW::~GLFW()
{
    glfwTerminate();
}

GLFW::GLFW()
    : window(nullptr), screenWidth(0), screenHeight(0)
{
    glfwInit();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
}

GLFW::GLFW(int width, int height) : GLFW()
{
    window = glfwCreateWindow(width, height, "Learn OpenGL", nullptr, nullptr);
    if(!window)
    {
        std::cout << "Failed to create OpenGL window\n";
        throw 1;
    }
    glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

GLfloat
GLFW::getAspect() const
{
    double w=screenWidth,h=screenHeight;
    return (GLfloat)(w/h);
}

namespace {
    Camera *cam;
    GLFW *cur;
    bool *keys;
    void KeyCallback(GLFWwindow *window, int key, int scancode, int action, int mode)
    {
        if(GLFW_KEY_ESCAPE == key && GLFW_PRESS == action)
            glfwSetWindowShouldClose(window, GL_TRUE);
        if(key >= 0 && key < 1024)
            keys[key] = (GLFW_PRESS == action || GLFW_REPEAT == action);
    }
    bool firstCall=true;
    void MouseCallback(GLFWwindow*, double xpos, double ypos)
    {
        if(firstCall)
        {
            cur->updateLast({xpos,ypos});
            firstCall = false;
        }
        glm::vec2 last = cur->getLast();
        glm::vec2 offset{(GLfloat)(xpos - last.x), (GLfloat)(last.y - ypos)};
        cur->updateLast({(GLfloat)xpos,(GLfloat)ypos});
        cam->updateLookAt(offset.x, offset.y);
    }
    void ScrollCallback(GLFWwindow*, double, double y_offset)
    {
        cam->updateZoom(y_offset);
    }
}

void
GLFW::setCallbacks(Camera *c)
{
    ::cam = c;
    ::cur = this;
    ::keys = this->keys;
    glfwSetKeyCallback(window, KeyCallback);
    glfwSetCursorPosCallback(window, MouseCallback);
    glfwSetScrollCallback(window, ScrollCallback);
}
