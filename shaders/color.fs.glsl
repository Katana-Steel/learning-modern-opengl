#version 440 core

struct Material
{
    sampler2D diffuse;
    sampler2D specular;
    float shine;
};

struct Light
{
    vec3 pos;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

out vec4 color;

in vec3 FragPos;
in vec3 Normal;
in vec2 MyTexture;

uniform vec3 viewPos;
uniform Material mat;
uniform Light light;

vec3 norm_lightDir()
{
    return normalize(light.pos - FragPos);
}

    // ambient light
vec3 ambient(vec3 m) {
    return light.ambient * m;
}

    // diffuse
vec3 diffuse(vec3 norm, vec3 ldir, vec3 m) {
    float d = max(dot(norm, ldir), 0.0);
    return light.diffuse * d * m;
}

    // specular
vec3 specular(vec3 norm, vec3 ldir, vec3 m) {
    vec3 rdir = reflect((-1.0 * ldir), norm);
    vec3 vdir = normalize(viewPos-FragPos);
    float shine = pow(max(dot(vdir, rdir), 0.0), mat.shine);
    return light.specular * shine * m;
}

void main() 
{
    vec3 norm = normalize(Normal);
    vec3 ldir = norm_lightDir();
    vec3 mdiff = vec3(texture(mat.diffuse, MyTexture));
    vec3 mspec = vec3(texture(mat.specular, MyTexture));
    color = vec4((ambient(mdiff) + diffuse(norm, ldir, mdiff) + specular(norm, ldir, mspec)), 1.0);
}
