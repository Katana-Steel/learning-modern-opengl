#version 440 core

in vec3 MyColor;
in vec2 MyTexure;

out vec4 color;

uniform sampler2D ourTexture1;

void main() 
{
    color = mix(texture(ourTexture1, MyTexure), vec4(Mycolor, 1.0), 0.3);
    // color = texture(ourTexture1, MyTexure);
}