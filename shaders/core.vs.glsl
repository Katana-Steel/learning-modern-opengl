#version 440 core
layout (location = 0) in vec3 pos;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoord;

out vec3 Normal;
out vec3 FragPos;
out vec3 MyColor;
out vec2 MyTexture;

uniform mat4 model = mat4(1.0);
uniform mat4 view = mat4(1.0);
uniform mat4 projection = mat4(1.0);



void main()
{
    gl_Position = projection * view * model * vec4(pos, 1.0);
    FragPos = vec3(model*vec4(pos,1.0));
    Normal = mat3(transpose(inverse(model))) * normal;
    MyColor = normal;
    MyTexture = vec2(texCoord.x, 1.0 - texCoord.y);
}
