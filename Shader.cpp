#include "Shader.hpp"
#include "Model.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <glm/gtc/type_ptr.hpp>

namespace {
    std::string readInFile(const char *path)
    {
        std::ifstream infile;
        std::stringstream rdbuf;
        infile.exceptions (std::ifstream::badbit);
        try {
            infile.open(path);
            rdbuf << infile.rdbuf();
            infile.close();
        }
        catch (const std::ifstream::failure &e)
        {
            std::cout << "ERROR::SHADER::FILE_READ_UNSUCCESSFUL\n" << path << std::endl;
            return "";
        }
        return rdbuf.str();
    }
}

GLuint Shader::compileShaderSource(const std::string &src, GLenum type) const
{
    const GLchar *sh_src = src.c_str();
    GLuint ret = glCreateShader( type );
    glShaderSource(ret, 1, &sh_src, nullptr);
    glCompileShader(ret);
    GLint success;
    glGetShaderiv(ret, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        GLchar info[512];
        glGetShaderInfoLog(ret, 512, nullptr, info);
        std::cout << "ERROR:SHADER::" << type << "::COMPILATION_FAILED\n";
        std::cout << info << '\n';
    }
    return ret;
}

Shader::Shader(const char* vertPath, const char* fragPath)
{
    GLuint vertShader = compileShaderSource(readInFile(vertPath), GL_VERTEX_SHADER);
    GLuint fragShader = compileShaderSource(readInFile(fragPath), GL_FRAGMENT_SHADER);
    program = glCreateProgram( );
    glAttachShader(program, vertShader);
    glAttachShader(program, fragShader);
    glLinkProgram(program);
    GLint success=0;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success)
    {
        GLchar info[512];
        glGetProgramInfoLog(program, 512, nullptr, info);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << info << '\n';
    }
    glDeleteShader(vertShader);
    glDeleteShader(fragShader);
}

GLint
Shader::lookupUniform(const char *name) const
{
    return glGetUniformLocation(program, name);
}

GLint
Shader::lookupAttrib(const char *name) const
{
    return glGetAttribLocation(program, name);
}

void
Shader::setMatrix(const char *name, const glm::mat4 &m4) const
{
    glProgramUniformMatrix4fv(program, lookupUniform(name),1,GL_FALSE, glm::value_ptr(m4));
}

void
Shader::setVec3(const char *name, const glm::vec3 &v) const
{
    glProgramUniform3fv(program, lookupUniform(name),1, glm::value_ptr(v));
}

void
Shader::setFloat(const char *name, GLfloat f) const
{
    glProgramUniform1f(program, lookupUniform(name), f);
}

void
Shader::setTextureID(const char *name, GLuint id) const
{
    glProgramUniform1i(program, lookupUniform(name), id);
}

#include <algorithm>
// ----- Model -----
void
Model::load(const char *path)
{
    std::string s = readInFile(path);
    if (s.size() < 1)
        return;
    vertices.reserve(std::count(s.begin(), s.end(), '.'));
    std::stringstream ssm{};
    ssm.str(s);
    ssm.seekg(0);
    ssm >> v >> c >> t;
    while(!ssm.eof())
    {
        GLfloat f;
        ssm >> f;
        vertices.push_back(f);
    }
}
