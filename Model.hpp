#pragma once
#include <vector>
#include <GL/glew.h>

struct Model {
    size_t stride() {return v+c+t;};
    uint16_t v;
    uint16_t c;
    uint16_t t;
    std::vector<GLfloat> vertices;
    size_t count() {return vertices.size() / stride();};
    void load(const char*fname);  // in shader.cpp 
};
